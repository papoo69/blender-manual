
#####################
  Weight Paint Mode
#####################

.. toctree::
   :maxdepth: 2

   introduction.rst
   tools.rst
   tool_settings/index.rst
   weights_menu.rst
